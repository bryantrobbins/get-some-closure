function pet(name, species, breed) {
	var _name = name;
	var _species = species;
	var _breed = breed;
	return {
		getName: function() {
			return _name;			
		}
	}
}

var pocket = pet('Pocket', 'dog', 'beagle');
console.log(pocket.getName());