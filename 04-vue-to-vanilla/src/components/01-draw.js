import SVG from 'svg.js';

export default function (canvasWidth, canvasHeight, el, updateColor) {
    var _obj = SVG(el);
    var _drawing = _obj.size(canvasWidth, canvasHeight);
    _drawing.click(function() {
        updateColor();
    });

    return {
        draw: function (width, height, color) {
            _drawing.rect(width, height).fill(color);
        },
        clear: function () {
            _drawing.clear();
        }
    }
}