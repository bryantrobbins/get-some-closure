function pet(name, species, breed, feed) {
	var _name = name;
	var _species = species;
	var _breed = breed;
	return {
		getName: function() {
			return _name;			
		},
		feed: function() {
			console.log(`START feeding ${_name}`);
			feed();
			console.log(`DONE feeding ${_name}`)
		}
	}
}

var foodBag = {
	remaining: 100
}

var pocket = pet('Pocket', 'dog', 'beagle', function() {
	foodBag.remaining -= 20;
});

// Dog simulator
console.log(`Initial food: ${foodBag.remaining}`);
pocket.feed();
pocket.feed();
console.log(`Food remaining: ${foodBag.remaining}`);