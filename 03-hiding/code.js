function pet(name, species, breed, feed) {
	var _name = name;
	var _species = species;
	var _breed = breed;
	return {
		getName: function() {
			return _name;			
		},
		feed: function() {
			console.log(`START feeding ${_name}`);
			feed();
			console.log(`DONE feeding ${_name}`)
		}
	}
}

function superFood(size) {
	var _remaining = size;
	return {
		getRemaining: function() {
			return _remaining;
		},
		pour: function(howMuch) {
			_remaining -= howMuch;
		}
	}
}

var foodBag = superFood(100);
var pocket = pet('Pocket', 'dog', 'beagle', function() {
	foodBag.pour(20);
});

// Dog simulator
console.log(`Initial food: ${foodBag.getRemaining()}`);
pocket.feed();
pocket.feed();
console.log(`Food remaining: ${foodBag.getRemaining()}`);