# get-some-closure

Some semi-practical examples of using closures in modern JavaScript.

Inspired by Kyle Simpson's You Don't Know JS volume on Scope and Closures,
available [here](https://github.com/getify/You-Dont-Know-JS/blob/master/scope%20%26%20closures/README.md).

Accompanying slides on [Google Slides](https://docs.google.com/presentation/d/1r_2ohCHR18Zk_jvO5TUY2Dyxde7svivff5TtQ3Wfcs0/edit?usp=sharing)