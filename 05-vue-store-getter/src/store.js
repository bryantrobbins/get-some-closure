import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pets: [
      { 
        name: 'Eddie',
        species: 'dog',
        breed: 'beagle',
        gender: 'male'
      },
      { 
        name: 'Pocket',
        species: 'dog',
        breed: 'beagle',
        gender: 'male'
      },
      { 
        name: 'Sugar',
        species: 'dog',
        breed: 'shorgi',
        gender: 'female'
      },
      {
        name: 'Sparkle',
        species: 'dog',
        breed: 'beagle',
        gender: 'female'
      },
      { 
        name: 'Honey',
        species: 'dog',
        breed: 'mixed',
        gender: 'female'
      },
      { 
        name: 'Blauser',
        species: 'dog',
        breed: 'dachshund',
        gender: 'male'
      },
      { 
        name: 'Sambo',
        species: 'rabbit',
        gender: 'male'
      }
    ]
  },
  getters: {
    getPetByName: (state) => (name) => {
      return state.pets.find(pet => pet.name === name)
    },
    getPetByNameHelpers: (state) => (name) => {
      var pet = state.pets.find(pet => pet.name === name);
      return {
        name: function() {
          return pet.name;
        },
        gender: function() {
          return pet.gender;
        },
        breedWithSpecies: function() {
          if(pet.breed) {
            return `${pet.breed} ${pet.species}`;
          } else {
            return pet.species;
          }
        },
        favorite: function() {
          if(pet.species === 'dog' && pet.breed === 'beagle' && pet.gender === 'female') {
            return "*";
          } else {
            return "";
          }
        }
      }
    }

  },
  mutations: {
  },
  actions: {
  }
})
